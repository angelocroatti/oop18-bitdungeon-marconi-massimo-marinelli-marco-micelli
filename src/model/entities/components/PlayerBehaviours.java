package model.entities.components;

/**
 * Lists the possible behaviours a player can have.
 */
public enum PlayerBehaviours {

    /**
     * The player is attacking.
     */
    ATTACKING,

    /**
     * The player is pacific.
     */
    PACIFIC
}
