***INSTRUCTIONS***
Move into the game world with keybord arrows.
Attack enemies with A button.
Collect potions in order to power-up your character for the final battle.

***DEPENDENCIES***
Please make sure to have java FX and java FXGL correctly installed.

***CREDITS***
All sprites animations belong to Gaurav Munjal (visit http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/)
The project makes use of the java FXGL open source library owned by Almas Baimagambetov (visit https://github.com/AlmasB/FXGL/wiki)
